/*eslint-disable*/

import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import "./assets/css/bootstrap.min.css"
import "./assets/scss/common.scss"
import VueLazyLoad from 'vue-lazyload'
Vue.use(VueLazyLoad)
import VModal from 'vue-js-modal'
 
Vue.use(VModal)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
