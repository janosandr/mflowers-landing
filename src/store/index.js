/*eslint-disable*/

import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios"

const URL = "https://zaktour.gov.ua/data.json";

Vue.use(Vuex)

let store = new Vuex.Store({
  state: {
    items: []
  },
  actions: {
   GET_DATA({commit}){
     return axios(URL, {method: "GET"})
     .then((items)=>{
       commit('SET_ITEMS_TO_STATE', items.data.data);
       console.log(items)
       return items;
     })
     .catch((error) => {
      console.log(error)
      return error;
    })
   }
  },
  getters:{
      ITEMS(state){
        return state.items;
      }
  },
  mutations: {
    SET_ITEMS_TO_STATE: (state, items) => {
      state.items = items;
    }
  },
  modules: {
  }
});

export default store;